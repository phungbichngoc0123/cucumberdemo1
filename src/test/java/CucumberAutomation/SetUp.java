package CucumberAutomation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class SetUp {

    WebDriver driver;

    public WebDriver setUpBrowser(String browser){
        if (browser.equalsIgnoreCase("Chrome")){
            System.setProperty("webdriver.chrome.driver", "D:\\driver\\chromedriver.exe");
            driver = new ChromeDriver();
        } else if (browser.equalsIgnoreCase("Firefox")){
                System.setProperty("webdriver.gecko.driver", "D:\\driver\\geckodriver.exe");
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("IE")){
            System.setProperty("webdriver.internetExplorer.driver", "D:\\driver\\IEDriverServer.exe");
            driver = new InternetExplorerDriver();
        } else {
            System.out.println("browser is invalid");
            driver = null;
        }
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
        return driver;
    }
}
