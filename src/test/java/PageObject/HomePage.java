package PageObject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

    WebDriver driver;
    private By avatar = By.xpath("//a[@class='user-profile dropdown-toggle']//span[@class='bg-avatar']");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getAvatar() {
        return driver.findElement(avatar);
    }

    public void verifyAvatarDisplay(){
        Assert.assertTrue(getAvatar().isDisplayed());
    }
}
