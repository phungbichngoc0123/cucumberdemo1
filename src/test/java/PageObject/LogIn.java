package PageObject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogIn {

    WebDriver driver;
    private By declineConsent = By.id("truste-consent-required");
    private By email = By.id("login-input-email");
    private By password = By.id("login-input-password");
    private By logInBtn = By.cssSelector("button[class='btn login-submit']");
    private By errorMessage = By.xpath("//p[contains(text(),'Hmm.. seems like you’ve got one of the fields wrong.')]");

    public LogIn(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getEmail() {
        return driver.findElement(email);
    }

    public WebElement getPassword() {
        return driver.findElement(password);
    }

    public WebElement getLogInBtn() {
        return driver.findElement(logInBtn);
    }

    public WebElement getErrorMessage() {
        return driver.findElement(errorMessage);
    }

    public WebElement getDeclineConsent(){
        return driver.findElement(declineConsent);
    }

    public void declineCookie(){
        getDeclineConsent().click();
    }

    public void logIn(String username, String password) throws InterruptedException {
        getEmail().sendKeys(username);
        getPassword().sendKeys(password);
        Thread.sleep(3000);
        getLogInBtn().click();
        Thread.sleep(5000);

//        JavascriptExecutor jse = (JavascriptExecutor)driver;
//        jse.executeScript("arguments[0].click", getLogInBtn());
    }

    public void verifyMessage(){
        Assert.assertTrue(getErrorMessage().isDisplayed());
    }

}
