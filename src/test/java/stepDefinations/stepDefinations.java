package stepDefinations;

import CucumberAutomation.SetUp;
import PageObject.HomePage;
import PageObject.LogIn;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.*;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(Cucumber.class)
public class stepDefinations extends SetUp {

    WebDriver driver;


    @Given("^user navigate to log in page \"([^\"]*)\"$")
    public void user_navigate_to_log_in_page_something(String url) throws Throwable {
        driver = setUpBrowser("Chrome");
        driver.manage().window().maximize();
        driver.get(url);
    }

    @When("^user log into lyf web with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void user_log_into_lyf_web_with_username_something_and_password_something(String inputEmail, String inputPass) throws Throwable {
        LogIn logInPage = new LogIn(driver);
        logInPage.declineCookie();
        logInPage.logIn(inputEmail, inputPass);
        Thread.sleep(3000);
    }

//    @When("^user log into lyf web with username (.+) and password (.+)$")
//    public void user_log_into_lyf_web_with_username_and_password (String inputEmail, String inputPass) throws Throwable {
//        LogIn logInPage = new LogIn(driver);
//        logInPage.logIn(inputEmail, inputPass);
//        Thread.sleep(3000);
//    }

    @Then("^username is displayed$")
    public void username_is_displayed() throws Throwable {
        HomePage hp = new HomePage(driver);
        hp.verifyAvatarDisplay();
    }

    @Then("error message is displayed")
    public void error_message_is_displayed() {
        // Write code here that turns the phrase above into concrete actions
        LogIn logInPage = new LogIn(driver);
        logInPage.verifyMessage();
    }


    @And("^browser is closed$")
    public void browser_is_closed() throws Throwable {
        driver.close();
    }


}
